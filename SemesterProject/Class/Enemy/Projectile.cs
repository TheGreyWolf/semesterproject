﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class.Enemy
{
    public class Projectile : Collider
    {
        private Texture2D sprite; // needs to be defined
        private Vector2 target;
        private float speed;
        private static List<GameObject> objectList = new List<GameObject>(); // Needs to be called to delete the objects. SystemHandler.cs maybe?
        /// <summary>
        /// Creates the projectile and tells it where to go towards.
        /// </summary>
        /// <param name="origin">The origin position</param>
        /// <param name="target">The target it shoots at</param>
        /// <param name="speed">How fast it moves towards the target</param>
        public Projectile(Vector2 origin, Vector2 target, float speed)
        {
            position = origin;
            this.target = target - origin;
            this.target.Normalize();
            this.speed = speed;
        }

        #region load stuff
        public override void LoadContent(ContentManager content)
        {
        }
        public override void Initialize()
        {

        }
        /// <summary>
        /// Updates the projectile status by moving it forward in the intended direction.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            position += target * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        /// <summary>
        /// Adds the projectile to the list to be deleted.
        /// </summary>
        public void Die()
        {
            objectList.Add(this);
        }
        #endregion
    }
}
