﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class
{
    public abstract class GameObject
    {
        public Vector2 position { get; set; } = new Vector2(300f, 1f);

        public abstract void LoadContent(ContentManager content);
        public abstract void Initialize();
        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
