﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class
{
    public abstract class Collider : GameObject
    {
        protected Texture2D _texture;
        public virtual Rectangle collisionBox {
            get
            {
                return new Rectangle(
                    (int)(position.X),
                    (int)(position.Y),
                    _texture.Width,
                    _texture.Height
                    );
            }

        }

        // This is called when stuff collides.
        public virtual void OnCollision(Collider other)
        {

        }

        // Lets check if stuff collides, if so we call OnCollision
        public virtual bool CheckCollision(Collider other)
        {
            if (IsColliding(other))
            {
                OnCollision(other);
                return true;
            }
            return false;
        }

        // If two items are colliding, return true
        public virtual bool IsColliding(Collider other)
        {
            return collisionBox.Intersects(other.collisionBox);
        }
    }
}
