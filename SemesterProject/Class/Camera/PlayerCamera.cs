﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SemesterProject.Class.System;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class.Camera
{
    public class PlayerCamera : GameObject
    {
        public Matrix Transform { get; private set; }
        #region load stuff
        public override void LoadContent(ContentManager content)
        {
        }
        public override void Initialize()
        {
            
        }
        public override void Update(GameTime gameTime)
        {

        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
        }
        #endregion

        public void Follow(Collider target)
        {
            var position = Matrix.CreateTranslation(
                -target.position.X - (target.collisionBox.Width / 2),
                -target.position.X - (target.collisionBox.Height / 2),
                0);

            var offset = Matrix.CreateTranslation(
                SystemHandler.ScreenHeight / 2,
                SystemHandler.ScreenWidth / 2,
                0);

            Transform = position * offset;
        }
        
    }
}
