﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SemesterProject.Class.Camera;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class.System
{
    public class SystemHandler
    {
        private PlayerCamera _camera;
        private Collider _player;
        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }
        public void LoadContent(ContentManager content, Collider player)
        {
            _camera = new PlayerCamera();
            _player = player;
        }
        public static void Initialize(GraphicsDeviceManager graphics)
        {
            ScreenHeight = graphics.PreferredBackBufferHeight;

            ScreenWidth = graphics.PreferredBackBufferWidth;
        }
        public void Update(GameTime gameTime)
        {
            _camera.Follow(_player);
        }
        public static void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }
    }
}
