﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace SemesterProject.Class.System
{
    public class ObstacleHandler
    {
        //texture of gameobject
        protected Texture2D _goTexture;

        //position and velocity of gameobjects
        public Vector2 _position;
        public Vector2 _velocity = new Vector2(0f, 0f);

        //When gameobjects collide
        public Rectangle _collisionRect
        {
            get
            {
                return new Rectangle(
                    (int)_position.X,
                    (int)_position.Y,
                    _goTexture.Width,
                    _goTexture.Height);
            }
        }

        //Gets gameobjects texture 
        public ObstacleHandler(Texture2D texture)
        {
            _goTexture = texture;
        }

        public virtual void Update(GameTime gameTime, List<ObstacleHandler> gameobject)
        {

        }

        //Draw gameobjects
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_goTexture, _position, Color.White);
        }

        #region Collision: Left, Right, Top, Bottom

        //Checks if gameobject is touching left side of other gameobject
        protected bool IsTouchingLeft(ObstacleHandler gameobject)
        {
            return this._collisionRect.Right + this._velocity.X > gameobject._collisionRect.Left &&
                         this._collisionRect.Left < gameobject._collisionRect.Left &&
                         this._collisionRect.Bottom > gameobject._collisionRect.Top &&
                         this._collisionRect.Top < gameobject._collisionRect.Bottom;
        }

        //Checks if gameobject is touching right side of other gameobject
        protected bool IsTouchingRight(ObstacleHandler gameobject)
        {
            return this._collisionRect.Left + this._velocity.X < gameobject._collisionRect.Right &&
                         this._collisionRect.Right > gameobject._collisionRect.Right &&
                         this._collisionRect.Bottom > gameobject._collisionRect.Top &&
                         this._collisionRect.Top < gameobject._collisionRect.Bottom;
        }

        //Checks if gameobject is touching top of other gameobject
        protected bool IsTouchingTop(ObstacleHandler gameobject)
        {
            return this._collisionRect.Bottom + this._velocity.Y > gameobject._collisionRect.Top &&
                         this._collisionRect.Top < gameobject._collisionRect.Top &&
                         this._collisionRect.Right > gameobject._collisionRect.Left &&
                         this._collisionRect.Left < gameobject._collisionRect.Right;
        }

        //Checks if gameobject is touching bottom of other gameobject
        protected bool IsTouchingBottom(ObstacleHandler gameobject)
        {
            return this._collisionRect.Top + this._velocity.Y < gameobject._collisionRect.Bottom &&
                         this._collisionRect.Bottom > gameobject._collisionRect.Bottom &&
                         this._collisionRect.Right > gameobject._collisionRect.Left &&
                         this._collisionRect.Left < gameobject._collisionRect.Right;
        }

        #endregion


        /*public static void LoadContent(ContentManager content)
        {
            
            foreach (Enemy enemy in _enemy)
            {
                Enemy.LoadContent(Content);
            }
            
        }*/
        /*public static void Initialize()
        {
            
            foreach (Enemy enemy in _enemy)
            {
                Enemy.Intialize();
            }
            
        }*/
        /*public static void Update(GameTime gameTime)
         {

             Enemy.Update(gameTime);
                 foreach (Enemy enemy in _enemy)
                 {
                     Enemy.CheckCollision(other);
                 }

         }*/
        /*public static void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

        }*/
    }
}
