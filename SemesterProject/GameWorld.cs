﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using SemesterProject.Class.Player;

namespace SemesterProject
{
    public class GameWorld : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Texture2D _texture;
        private Vector2 _position;

        //private Player _player;

        public GameWorld()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()                            // TODO: Add your initialization logic here
        {
            base.Initialize();
        }

        protected override void LoadContent()                           // TODO: use this.Content to load your game content here
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

           // var _texture = Content.Load<Texture2D>("Still"); // Load Picture "Still" -

           // _player = new Player(_texture);                  // - at "_player" location, -
            //_player._position = new Vector2(400, 200);       // - which is at this Vector2D X,Y value.
        }

        protected override void Update(GameTime gameTime)               // TODO: Add your update logic here
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) //if ESC is pressed, then -
                Exit();                                                                                                              // - Exit program.

           // _player.Update();


            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)                 // TODO: Add your drawing code here
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

           // _spriteBatch.Begin();

            //_player.Draw(_spriteBatch);

           // _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
